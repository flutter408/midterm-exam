import 'package:flutter/material.dart';
import './routes/pageRoute.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              '[63160201] นราชัย ประไพลศาล',
              style: TextStyle(fontSize: 19),
            ),
            accountEmail: Text(
              'วิทยาการคอมพิวเตอร์',
              style: TextStyle(fontSize: 17),
            ),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  'https://i.seadn.io/gae/N5hLf_L1l4IWEILp8gXn7O70AjTtyMGJJgyFgU6T3FGwgoFsR5IXmiIyPA5eXSbcNys-WXfkbLPfgssR74_rPY-XblaZVEN2YZ-V?auto=format&w=1000',
                  fit: BoxFit.cover,
                  width: 90,
                  height: 90,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.amber,
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('หน้าหลัก'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.home),
          ),
          ListTile(
            leading: Icon(Icons.grid_on),
            title: Text('ตารางเรียน/สอบ'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.table),
          ),
          ListTile(
            leading: Icon(Icons.book_outlined),
            title: Text('ลงทะเบียนเรียน'),
            onTap: () => showDialog(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                      content: const Text(
                          'นิสิตทำการลงทะเบียนแล้วโปรดตรวจสอบรายวิชาที่เมนูผลการลงทะเบียน'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'OK'),
                          child: const Text('OK'),
                        ),
                      ],
                    )),
          ),
          ListTile(
            leading: Icon(Icons.receipt_outlined),
            title: Text('ผลการศึกษา'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.grade),
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text('ประวัตินิสิต'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.biblio),
          ),
          ListTile(
            leading: Icon(Icons.image),
            title: Text('ตรวจสอบรูปนิสิต'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.img),
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app, color: Colors.red),
            title: Text('ออกจากระบบ', style: TextStyle(color: Colors.red)),
            onTap: () =>showDialog(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                      content: const Text(
                          'คุณต้องการออกจากระบบหรือไม่ ?'),
                      actions: <Widget>[ 
                        TextButton(
                          onPressed: () => Navigator.pop(context),
                          child: const Text('ยกเลิก'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.pushReplacementNamed(context, pageRoutes.login),
                          child: const Text('ยืนยัน',style: TextStyle(color: Colors.red),),
                        ),
                      ],
                    )),
          ),
          
        ],
      ),
    );
  }
}
