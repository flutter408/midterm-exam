import 'package:flutter/material.dart';
import 'Sidemenu.dart';

class imgPage extends StatelessWidget {
 static const String routeName = '/imgPage';

 @override
 Widget build(BuildContext context) {
   return new Scaffold(
       appBar: AppBar(
         title: Text("Contacts"),
       ),
       drawer: NavDrawer(),
       body: Center(child: Image.network(
    "https://reg.buu.ac.th/registrar/getstudentimage.asp?id=63160183",
    fit: BoxFit.cover,
  ))
       );
 }
}