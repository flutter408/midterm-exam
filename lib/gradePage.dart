import 'package:flutter/material.dart';
import 'Sidemenu.dart';

class gradePage extends StatelessWidget {
  static const String routeName = '/gradePage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("ผลการศึกษา"),
        ),
        drawer: NavDrawer(),
        body: ListView(
          children: <Widget>[
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        '63160201 : นายนราชัย ประไพลศาล : คณะวิทยาการสารสนเทศ'),
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        'หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -'),
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text('สถานภาพ: กำลังศึกษา'),
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        'อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน'),
                  ),
                )
              ],
            )),
            const Divider(
              height: 20,
              thickness: 1,
              indent: 0,
              endIndent: 0,
              color: Colors.black,
            ),
            Container(
                child: Column(
              children: <Widget>[
                Text(
                  'ภาคการศึกษาที่ 1/2565',
                  style: TextStyle(fontSize: 20),
                ),
              ],
            )),
            Center(
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(20),
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(120.0),
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      columnWidths: {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(5),
                        2: FlexColumnWidth(2),
                        3: FlexColumnWidth(2),
                      },
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Text('รหัสวิชา', style: TextStyle(fontSize: 18.0))
                          ]),
                          Column(children: [
                            Text('ชื่อรายวิชา',
                                style: TextStyle(fontSize: 18.0))
                          ]),
                          Column(children: [
                            Text('หน่วยกิต', style: TextStyle(fontSize: 18.0))
                          ]),
                          Column(children: [
                            Text('เกรด', style: TextStyle(fontSize: 18.0))
                          ])
                        ]),
                        TableRow(children: [
                          Column(children: [Text('67511164')]),
                          Column(children: [
                            Text(
                                'Contemporary Public Sector Administration in Thailand')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('A')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88624259')]),
                          Column(
                              children: [Text('Mobile Programming Paradigm')]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('B')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88631159')]),
                          Column(children: [
                            Text('Algorithm Design and Applications')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('D+')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88633159')]),
                          Column(children: [Text('Computer Networks')]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('C+')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88634159')]),
                          Column(children: [Text('Software Development')]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('A')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88635359')]),
                          Column(children: [
                            Text('User Interface Design and Development')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('B+')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88636159')]),
                          Column(children: [
                            Text('Introduction to Artificial Intelligence')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text('B+')]),
                        ]),
                      ],
                    ))
              ]),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Text(
                  'ภาคการศึกษาที่ 2/2565',
                  style: TextStyle(fontSize: 20),
                ),
              ],
            )),
            Center(
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(20),
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(120.0),
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      columnWidths: {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(5),
                        2: FlexColumnWidth(2),
                        3: FlexColumnWidth(2),
                      },
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Text('รหัสวิชา', style: TextStyle(fontSize: 18.0))
                          ]),
                          Column(children: [
                            Text('ชื่อรายวิชา',
                                style: TextStyle(fontSize: 18.0))
                          ]),
                          Column(children: [
                            Text('หน่วยกิต', style: TextStyle(fontSize: 18.0))
                          ]),
                          Column(children: [
                            Text('เกรด', style: TextStyle(fontSize: 18.0))
                          ])
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88624359')]),
                          Column(children: [Text('Web Programming')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text(' ')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88624459')]),
                          Column(children: [
                            Text('Object-Oriented Analysis and Design')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text(' ')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88624559')]),
                          Column(children: [Text('Software Testing')]),
                          Column(children: [Text('3')]),
                          Column(children: [Text(' ')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88634259')]),
                          Column(children: [
                            Text('Multimedia Programming for Multiplatforms')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text(' ')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88634459')]),
                          Column(children: [
                            Text('Mobile Application Development I')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text(' ')]),
                        ]),
                        TableRow(children: [
                          Column(children: [Text('88646259')]),
                          Column(children: [
                            Text('Introduction to Natural Language Processing')
                          ]),
                          Column(children: [Text('3')]),
                          Column(children: [Text(' ')]),
                        ]),
                      ],
                    ))
              ]),
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    'การแสดงเกรด',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                  Text(
                      '1. กรณีที่นิสิตประเมินครบทุกรายวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะเป็นเกรดที่ได้',
                      style: TextStyle(fontSize: 15, color: Colors.red)),
                  Text(
                      "2. กรณีที่นิสิตประเมินครบทุกวิชาแล้ว แต่เกรดยังไม่ออก ช่องแสดงเกรดจะเป็นช่องว่าง",
                      style: TextStyle(fontSize: 15, color: Colors.red)),
                  Text(
                      '3. กรณีที่นิสิตประเมินไม่ครบทุกวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะมีเครื่องหมาย ??',
                      style: TextStyle(fontSize: 15, color: Colors.red)),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    'หมายเหตุ',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                      '- CA= Credit Attempt, GP= Point , GPA =GP/CA Grade point average',
                      style: TextStyle(fontSize: 15)),
                  Text(
                      "- C.Register=หน่วยกิตลงทะเบียน , C.Earn=หน่วยกิตที่ได้รับ , CA=หน่วยกิตคำนวน ,GP=ค่าคะแนน ,GPA=ค่าคะแนนเฉลี่ย",
                      style: TextStyle(fontSize: 15)),
                  Text(
                      '- โปรแกรมทดสอบเกรด จะยังไม่นำผลของการ Regrade F ไปคำนวณจนกว่าจะได้รับเกรดจริง',
                      style: TextStyle(fontSize: 15)),
                ],
              ),
            ),
          ],
        ));
  }
}
