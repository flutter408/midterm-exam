import 'dart:html';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import './routes/pageRoute.dart';
import 'Sidemenu.dart';
import 'package:fluttertoast/fluttertoast.dart';

class loginPage extends StatelessWidget {
  static const String routeName = '/loginPage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("เข้าสู่ระบบมหาวิทยาลัยบูรพา"),
          automaticallyImplyLeading: false,
        ),
        //  drawer: NavDrawer(),
        body: ListView(
          children: <Widget>[
            Container(
              child: Center(
                child: Column(
                  children: <Widget>[
                    Image.asset('images/logo_buu-03.png',
                        fit: BoxFit.cover, height: 200),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                          focusedBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black),
                          ),
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.black)),
                          border: OutlineInputBorder(),
                          labelStyle: new TextStyle(color: Colors.black),
                          labelText: 'รหัสนิสิต',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        obscureText: true,
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                          focusedBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.black),
                          ),
                          enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.black)),
                          border: OutlineInputBorder(),
                          labelStyle: new TextStyle(color: Colors.black),
                          labelText: 'รหัสผ่าน',
                        ),
                      ),
                    ),
                    OutlinedButton(
                      child: Text(
                        "  เข้าสู่ระบบ  ",
                        style: TextStyle(fontSize: 20),
                      ),
                      style: OutlinedButton.styleFrom(
                        fixedSize: Size(250, 50),
                        padding: EdgeInsets.all(10),
                        primary: Colors.black,
                        side: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                      onPressed: () => Navigator.pushReplacementNamed(
                          context, pageRoutes.home),
                    ),
                    RichText(
                      text: TextSpan(
                        text: '\nลืมรหัสผ่าน ?',
                        children: [
                          TextSpan(
                              text: ' คลิกที่นี้',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () => showDialog(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        AlertDialog(
                                          title:
                                              const Text('ลืมรหัสผ่าน ?'),
                                          content: const Text(
                                              'สามารถเข้าแก้ไขได้ที่ https://myid.buu.ac.th/'),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () =>
                                                  Navigator.pop(context, 'OK'),
                                              child: const Text('OK'),
                                            ),
                                          ],
                                        )
                                        )
                                        ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
