import 'package:flutter/material.dart';
import 'Sidemenu.dart';

class tablePage extends StatelessWidget {
  static const String routeName = '/tablePage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("ตารางเรียน/สอบ"),
        ),
        drawer: NavDrawer(),
        body: ListView(
          children: <Widget>[
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        '63160201 : นายนราชัย ประไพลศาล : คณะวิทยาการสารสนเทศ'),
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        'หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -'),
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text('สถานภาพ: กำลังศึกษา'),
                  ),
                )
              ],
            )),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        'อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน'),
                  ),
                )
              ],
            )),
            const Divider(
              height: 20,
              thickness: 1,
              indent: 0,
              endIndent: 0,
              color: Colors.black,
            ),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: RichText(
                      text: TextSpan(
                        text: 'ปีการศึกษา',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        children: const <TextSpan>[
                          TextSpan(
                              text: ' 〈 ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue)),
                          TextSpan(
                              text: '2565',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red)),
                          TextSpan(
                              text: ' 〉',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue)),
                          TextSpan(
                              text: '/',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red)),
                          TextSpan(
                              text: ' 1 ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue)),
                          TextSpan(
                              text: ' 2 ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red)),
                          TextSpan(
                              text: ' ฤดูร้อน ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue)),
                          TextSpan(
                              text: ' ระหว่าง ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              )),
                          TextSpan(
                              text: ' 〈 ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue)),
                          TextSpan(
                              text: '23/1/66 - 29/1/66',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red)),
                          TextSpan(
                              text: ' 〉',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue)),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )),
            Container(
              child: Image.asset('images/table.png'),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(
                        '\n* ข้อมูลที่ปรากฎอยู่ในตารางเรียนประกอบด้วย รหัสวิชา (จำนวนหน่วยกิต) กลุ่ม, ห้องเรียนและอาคาร ตามลำดับ\n',style: TextStyle(fontSize: 10),),
                  ),
                )
              ],
            )),
            Container(
              child: Image.asset('images/table2.png'),
            ),
          ],
        ));
  }
}
