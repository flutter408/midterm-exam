import 'package:flutter/material.dart';
import 'Sidemenu.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';


class homePage extends StatelessWidget {
  static const String routeName = '/homePage';

  @override
  Widget build(BuildContext context) {
    // ignore: unnecessary_new
    return new Scaffold(
        appBar: AppBar(
          title: Text("มหาวัทยาลัยบูรพา"),
        ),
        drawer: NavDrawer(),
        body: ListView(
          children: <Widget>[
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(' ประกาศต่างๆ',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                  ),
                )
              ],
            )),
            Divider(),
            ImageSlideshow(
              width: double.infinity,
              height: 580,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          showImageViewer(
                              context, Image.asset("images/grad652.png").image,
                              swipeDismissible: false);
                        },
                        child: Container(
                          height: 200,
                          child: Image.asset(
                            "images/grad652.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text(
                        "การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565 (ด่วน)",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '\n      นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงินตามกำหนดเวลาหากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทางกองทะบียนฯ จะไม่เสนอชื่อสำเร็จการศึกษาคู่มือการยื่นคำร้องสำเร็จการศึกษา นิสิตพิมพ์ใบชำระเงินที่เมนูแจ้งจบและขึ้นทะเบียนนิสิตนิสิตสิตจะรับเอกสารจบการศึกษาได้ต่อเมื่อนิสิตมีสถานะจบการศึกษาโดยตรวจสอบที่เมนูตรวจสอบวันที่สำเร็จการศึกษา',
                        style: TextStyle(fontSize: 13),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          showImageViewer(
                              context, Image.asset("images/pay652.jpg").image,
                              swipeDismissible: false);
                        },
                        child: Container(
                          height: 200,
                          child: Image.asset(
                            "images/pay652.jpg",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text(
                        "กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '\n      สำหรับการ scan ชำระค่าธรรมเนียมการศึกษา แอปธนาคารกสิกรไทย ไม่สามารถชำระได้ ให้ใช้แอปของธนาคารอื่นๆ ขออภัยในความไม่สะดวกครับ',
                        style: TextStyle(fontSize: 13),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          showImageViewer(context,
                              Image.asset("images/graduatePic.jpg").image,
                              swipeDismissible: false);
                        },
                        child: Container(
                          height: 200,
                          child: Image.asset(
                            "images/graduatePic.jpg",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text(
                        "กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชทานปริญญาบัตรปีการศึกษา 2563 และปีการศึกษา 2564",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '\n      1.นิสิตที่ประสงค์ขอเปลี่ยนคำนำหน้าคำอ่านเพื่อแต่งชุดปกติขาวตามชั้นยศในวันพิธีให้นิสิตยื่นความประสงค์ที่ลิงค์ คลิกที่นี่\n*** ผู้ที่สำเร็จการศึกษาปี 2563 ยื่นเอกสารไม่เกิน วันที่ 3 ก.พ. 2566***\n*** ผู้ที่สำเร็จการศึกษาปี 2564 ยื่นเอกสารไม่เกิน วันที่ 31 ก.ค. 2566***\n2. นิสิตที่ประสงค์ขอรับเป็นภิกษุในวันพิธี ให้นิสิตยื่นความประสงค์ที่ลิงค์\n*** ผู้ที่สำเร็จการศึกษาปี 2563 ยื่นเอกสารไม่เกิน วันที่ 31 ต.ค. 2565***\n*** ผู้ที่สำเร็จการศึกษาปี 2564 ยื่นเอกสารไม่เกิน วันที่ 31 ก.ค. 2566***\n3. นิสิตที่ประสงค์รับเป็นบัณฑิตพิเศษ ให้นิสิตยื่นความประสงค์ที่ลิงค์\n*** ผู้ที่สำเร็จการศึกษาปี 2563 ยื่นเอกสารไม่เกิน วันที่ 3 ก.พ. 2566 ***\n*** ผู้ที่สำเร็จการศึกษาปี 2564 ยื่นเอกสารไม่เกิน วันที่ 31 ก.ค. 2566**\n4. นิสิตที่ประสงค์ขอแต่งกายตามเพศที่แสดงออกในวันพิธี\n(เฉพาะเพศชายแต่งเป็นหญิง หรือ เพศหญิงแต่งเป็นชาย) ให้นิสิตยื่นความประสงค์ที่ลิงค์\n*** ผู้ที่สำเร็จการศึกษาปี 2563 ยื่นเอกสารไม่เกิน วันที่ 3 ก.พ. 2566 *(ขยายเวลา)\n*** ผู้ที่สำเร็จการศึกษาปี 2564 ยื่นเอกสารไม่เกิน วันที่ 31 ก.ค. 2566**',
                        style: TextStyle(fontSize: 13),
                      )
                    ],
                  ),
                ),
              ],
              onPageChanged: (value) {
                print('Page changed: $value');
              },
              autoPlayInterval: 15000,
              isLoop: true,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                DatePicker(
                  DateTime.now(),
                  initialSelectedDate: DateTime.now(),
                  selectionColor: Colors.black,
                  selectedTextColor: Colors.white,
                ),
              ],
            ),
            Container(
              child: Image.asset('images/Line.jpg'),
            ),
            Column(
              children: <Widget>[
                Text('\nมหาวิทยาลัยบูรพา | สกอ. | กยศ. | สมศ. | Vision Net Co.Ltd. | ')
              ],
            )
          ],
        ));
  }
}

