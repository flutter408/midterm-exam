import 'package:flutter/material.dart';
import 'Sidemenu.dart';

class biblioPage extends StatelessWidget {
  static const String routeName = '/biblioPage';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("ประวิตินิสิต"),
        ),
        drawer: NavDrawer(),
        body: ListView(
          children: <Widget>[
            Container(
              child: Column(children: [
                Text(
                  'ระเบียนประวัติ',
                  style: TextStyle(fontSize: 30, color: Colors.orange),
                )
              ]),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(' ข้อมูลด้านการศึกษา',
                        style: TextStyle(fontSize: 18)),
                  ),
                )
              ],
            )),
            Center(
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(20),
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(120.0),
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      columnWidths: {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(5),
                      },
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' รหัสประจำตัว:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 63160201',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' เลขที่บัตรประชาชน:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 1619900375184',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ชื่อ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' นายนราชัย ประไพลศาล',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ชื่ออังกฤษ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' MR.NARACHAI PRAPHAISAN',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' คณะ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' คณะวิทยาการสารสนเทศ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' วิทยาเขต:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' บางแสน',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' หลักสูตร:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' วิชาโท:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' -',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ระดับการศึกษา:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ปริญญาตรี',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ชื่อปริญญา:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ปีการศึกษาที่เข้า:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 2563 / 1 วันที่ 21/5/2563',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  'สถานภาพ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' วิธีรับเข้า:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' รับตรงทั่วประเทศ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  'วุฒิก่อนเข้ารับการศึกษา:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ม.6\n ? ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' จบการศึกษาจาก:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' บ้านทุ่งนาวิทยา',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' อ. ที่ปรึกษา:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน',
                                ),
                              ),
                            )
                          ]),
                        ]),
                      ],
                    ))
              ]),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(' ผลการศึกษา ', style: TextStyle(fontSize: 18)),
                  ),
                )
              ],
            )),
            Center(
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(20),
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(120.0),
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      columnWidths: {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(5),
                      },
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' หน่วยกิตคำนวณ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 93',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' หน่วยกิตที่ผ่าน:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 93',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' คะแนนเฉลี่ยสะสม:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 3.56',
                                ),
                              ),
                            )
                          ]),
                        ]),
                      ],
                    ))
              ]),
            ),
            Container(
                child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(' ข้อมูลส่วนบุคคล ', style: TextStyle(fontSize: 18)),
                  ),
                )
              ],
            )),
            Center(
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(20),
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(120.0),
                      border: TableBorder.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1),
                      columnWidths: {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(5),
                      },
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' สัญชาติ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ไทย',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ศาสนา:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' พุทธ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' หมู่เลือด:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' B',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ที่อยู่:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' - วังหิน เขต/อำเภอ บ้านไร่ อุทัยธานี 61180 โทร: 0910255312',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ความพิการ:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' สมอง',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' สถานภาพการรับทุน:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ไม่ได้รับทุน',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ความต้องการทุนการศึกษา:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' ต้องการ',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' จำนวนพี่น้องทั้งหมด(รวมตัวเอง):',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 3',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' นิสิตเป็นบุตรคนที่:',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 2',
                                ),
                              ),
                            )
                          ]),
                        ]),
                        TableRow(children: [
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' จำนวนพี่น้องที่กำลังศึกษาอยู่ (รวมตัวเอง):',
                                ),
                              ),
                            )
                          ]),
                          Column(children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  ' 2',
                                ),
                              ),
                            )
                          ]),
                        ]),
                      ],
                    ))
              ]),
            ),
          ],
        ));
  }
}
