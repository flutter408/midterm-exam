import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:regmidterm/biblioPage.dart';
import 'package:regmidterm/gradePage.dart';
import 'package:regmidterm/loginPage.dart';
import 'package:regmidterm/time_tablePage.dart';
import './routes/pageRoute.dart';
import 'Sidemenu.dart';
import 'homePage.dart';
import 'imgPage.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.amber,
        ),
        home: loginPage(),
        routes: {
          pageRoutes.home: (context) => homePage(),
          pageRoutes.biblio: (context) => biblioPage(),
          pageRoutes.grade: (context) => gradePage(),
          pageRoutes.table: (context) => tablePage(),
          pageRoutes.login: (context) => loginPage(),
          pageRoutes.img: (context) => imgPage(),
        },
      ),
    );
  }
}
