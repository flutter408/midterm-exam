import 'package:regmidterm/loginPage.dart';

import '../homePage.dart';
import '../biblioPage.dart';
import '../gradePage.dart';
import '../time_tablePage.dart';
import '../imgPage.dart';

class pageRoutes {
 static const String home = homePage.routeName;
 static const String biblio = biblioPage.routeName;
 static const String grade = gradePage.routeName;
 static const String table = tablePage.routeName;
 static const String login = loginPage.routeName;
 static const String img = imgPage.routeName;

}